package com.example.luislinietsky.clase5listas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    private String[] nombres = new String[]{"Juan", "Pablo", "Maria", "José", "Leonardo", "Luis", "Matias", "Jacinto", "Eustaquio", "Pamela", "Rodolfo", "Santiago", "Emiliano", "Alvaro", "Ezequiel", "Micaela", "Sofia", "Ricardo", "Laura", "Paola", "Cintia", "Daniel", "Tomás", "Sabrina", "Julieta", "Romina", "Valeria"};

    private BaseAdapter adapter = new BaseAdapter() {

        @Override
        public int getCount() {
            return nombres.length;
        }

        @Override
        public String getItem(int i) {
            return nombres[i];
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int index, View view, ViewGroup parent) {


            if(view == null){
                LayoutInflater li = getLayoutInflater();
                view = li.inflate(R.layout.list_item_nombre, parent, false);
                Log.d("Prueba De Lista", "Estamos creando item nuevo para " + index);
            }else{
                TextView textview = (TextView) view.findViewById(R.id.nombre);
                String nombreViejo = textview.getText().toString();
                Log.d("Prueba De Lista", "Estamos reciclando una vista para " + index + " (" + nombreViejo + ")");
            }

            TextView textview = (TextView) view.findViewById(R.id.nombre);
            String nombre = getItem(index);
            textview.setText(nombre);

            return view;
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView lista = (ListView)findViewById(R.id.lista);
        lista.setAdapter(adapter);

    }
}
