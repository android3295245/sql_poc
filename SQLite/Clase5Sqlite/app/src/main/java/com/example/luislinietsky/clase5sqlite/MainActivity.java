package com.example.luislinietsky.clase5sqlite;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.buscar_viejo).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DbHelper dbHelper = new DbHelper(MainActivity.this);
                SQLiteDatabase db = dbHelper.getReadableDatabase();

                Cursor cursor = db.rawQuery("SELECT * FROM usuario ORDER BY fnac ASC LIMIT 2", null);
                Integer[] idDeLabels = new Integer[]{R.id.textView, R.id.textView2};
                cursor.moveToFirst();

                int cantidadDeResultados = cursor.getCount();
                
                if(cantidadDeResultados == 0 ){
                    Toast.makeText(MainActivity.this, "No hay usuarios cargados", Toast.LENGTH_SHORT).show();
                    return;
                }

                int i = 0 ;
                String mensageStr = "";
                while(cursor.moveToNext()) {
                    String nombre = cursor.getString(cursor.getColumnIndex("nombre"));
                    String apellido = cursor.getString(cursor.getColumnIndex("apellido"));
                    TextView mensage = (TextView)findViewById(idDeLabels[i++]);
                    mensage.setText("El usuario mas viejo es '" + nombre + " " + apellido + "'" );
                }
                cursor.close();
                db.close();
            }
        });
    }
}
