package com.example.luislinietsky.clase5sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Luis Linietsky on 14/10/2016.
 */
public class DbHelper extends SQLiteOpenHelper{

    private static final int VERSION_DE_BASE_DE_DATOS = 2;

    private static final String SQL_CREATE_TABLE_USUARIO = "CREATE TABLE usuario(\n" +
            "\n" +
            "\tid integer not null primary key AUTOINCREMENT , \n" +
            "\tnombre varchar(64) NOT NULL,\n" +
            "\tapellido varchar(64) NOT NULL,\n" +
            "\tfnac date NOT NULL,\n" +
            "\tdni varchar(16),\n" +
            "\tdireccion varchar(256)\n" +
            "\n" +
            ")";

    private static final String SQL_INITIAL_DATA = "insert into usuario (nombre, apellido, fnac) values \n" +
            "('Luis', 'Linietsky', '1985-01-12'),\n" +
            "('Juan', 'Lopez', '1985-08-12'),\n" +
            "('Maria', 'Sarasa', '1995-08-12')";


    public DbHelper(Context context) {
        super(context, "usuarios.db", null, VERSION_DE_BASE_DE_DATOS);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_USUARIO);
        sqLiteDatabase.execSQL(SQL_INITIAL_DATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE usuario;");
        sqLiteDatabase.execSQL(SQL_CREATE_TABLE_USUARIO);
        sqLiteDatabase.execSQL(SQL_INITIAL_DATA);
    }
}
